package com.incentro.newsletter.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.incentro.newsletter.models.Newsletter;

public interface NewsletterRepository extends JpaRepository<Newsletter, Long> {
}
