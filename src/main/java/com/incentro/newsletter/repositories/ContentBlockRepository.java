package com.incentro.newsletter.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.incentro.newsletter.models.ContentBlock;

public interface ContentBlockRepository extends JpaRepository<ContentBlock, Long> {
}
