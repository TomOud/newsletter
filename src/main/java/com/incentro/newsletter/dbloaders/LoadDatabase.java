package com.incentro.newsletter.dbloaders;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.incentro.newsletter.models.ContentBlock;
import com.incentro.newsletter.models.Newsletter;
import com.incentro.newsletter.repositories.NewsletterRepository;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
class LoadDatabase {

    @Bean
    CommandLineRunner initDatabase(NewsletterRepository newsletterRepository) {
        return args -> {
            final List<Newsletter> newsletters = createNewsletters();
            for (Newsletter newsletter : newsletters) {
                log.info("Preloading " + newsletterRepository.save(newsletter));
            }
        };
    }

    /**
     * Creates newsletters to initialize the database
     *
     * @return a list of newsletters
     */
    List<Newsletter> createNewsletters() {

        // Create a set of content blocks for the newsletter of january
        Set<ContentBlock> contentBlocks1 = new HashSet<ContentBlock>() {
            {
                add(new ContentBlock("Heading", "Subject", "Body with content."));
                add(new ContentBlock("Heading of another block", "Subject two", "Body with a bit more content."));
            }
        };

        // Create a set of content blocks for the newsletter of february
        Set<ContentBlock> contentBlocks2 = new HashSet<ContentBlock>() {
            {
                add(new ContentBlock("February news", "Subject three", "Body with content of february."));
                add(new ContentBlock("Another block february", "Subject four", "Body of another block in february."));
            }
        };

        // Create and return the actual newsletters containing their own content blocks
        return new ArrayList<Newsletter>() {
            {
                add(new Newsletter("Monthly newsletter: january", LocalDate.now(), contentBlocks1));
                add(new Newsletter("Monthly newsletter: february", LocalDate.now().plusDays(1), contentBlocks2));
            }
        };
    }
}