package com.incentro.newsletter.advices;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.incentro.newsletter.exceptions.NewsletterNotFoundException;

@ControllerAdvice
public class NewsletterAdvice {

    @ResponseBody
    @ExceptionHandler(NewsletterNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String newsletterNotFoundHandler(NewsletterNotFoundException ex) {
        return ex.getMessage();
    }
}
