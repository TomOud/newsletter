package com.incentro.newsletter.resourceassemblers;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import com.incentro.newsletter.controllers.NewsletterController;
import com.incentro.newsletter.models.Newsletter;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@Component
public class NewsletterResourceAssembler implements ResourceAssembler<Newsletter, Resource<Newsletter>> {

    @Override
    public Resource<Newsletter> toResource(final Newsletter newsletter) {
        return new Resource<>(newsletter,
                linkTo(methodOn(NewsletterController.class).one(newsletter.getId())).withSelfRel(),
                linkTo(methodOn(NewsletterController.class).all()).withRel("newsletters"));
    }
}
