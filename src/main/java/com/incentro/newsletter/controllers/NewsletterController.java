package com.incentro.newsletter.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.incentro.newsletter.exceptions.NewsletterNotFoundException;
import com.incentro.newsletter.models.Newsletter;
import com.incentro.newsletter.repositories.NewsletterRepository;
import com.incentro.newsletter.resourceassemblers.NewsletterResourceAssembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@RestController
public class NewsletterController {
    private final NewsletterRepository repository;
    private final NewsletterResourceAssembler assembler;

    NewsletterController(NewsletterRepository repository, NewsletterResourceAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    // Aggregate root
    @GetMapping("/newsletters")
    public Resources<Resource<Newsletter>> all() {

        List<Resource<Newsletter>> newsletters = repository.findAll().stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(newsletters,
                linkTo(methodOn(NewsletterController.class).all()).withSelfRel());
    }

    @PostMapping("/newsletters")
    Newsletter newNewsletter(@RequestBody Newsletter newNewsletter) {
        return repository.save(newNewsletter);
    }

    // Single item
    @GetMapping("/newsletters/{id}")
    public Resource<Newsletter> one(@PathVariable Long id) {

        Newsletter newsletter = repository.findById(id)
                .orElseThrow(() -> new NewsletterNotFoundException(id));

        return assembler.toResource(newsletter);
    }

    @PutMapping("/newsletters/{id}")
    public Newsletter replaceNewsletter(@RequestBody Newsletter newNewsletter, @PathVariable Long id) {

        return repository.findById(id)
                .map(newsletter -> {
                    newsletter.setTitle(newNewsletter.getTitle());
                    return repository.save(newsletter);
                })
                .orElseGet(() -> {
                    newNewsletter.setId(id);
                    return repository.save(newNewsletter);
                });
    }

    @DeleteMapping("/newsletters/{id}")
    public void deleteNewsletter(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
