package com.incentro.newsletter.models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@Entity
@AllArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(exclude = "newsletter")
@ToString(exclude = "newsletter")
public class ContentBlock {

    @Id
    @GeneratedValue
    private Long id;
    private String heading;
    private String subject;
    private String body;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn
    @JsonIgnoreProperties("contentBlocks")
    private Newsletter newsletter;

    public ContentBlock(final String heading, final String subject, final String body) {
        this.heading = heading;
        this.subject = subject;
        this.body = body;
    }
}
