package com.incentro.newsletter.models;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "contentBlocks")
public class Newsletter {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private LocalDate date;

    @OneToMany(mappedBy = "newsletter", cascade = CascadeType.ALL)
    @JsonIgnoreProperties("newsletter")
    private Set<ContentBlock> contentBlocks;

    public Newsletter(final String title, final LocalDate date, final ContentBlock... contentBlocks) {
        this(title, date, Stream.of(contentBlocks).collect(Collectors.toSet()));
    }

    public Newsletter(final String title, final LocalDate date, final Set<ContentBlock> contentBlocks) {
        this.title = title;
        this.date = date;
        this.contentBlocks = contentBlocks;
        this.contentBlocks.forEach(x -> x.setNewsletter(this));
    }
}
