package com.incentro.newsletter.exceptions;

public class NewsletterNotFoundException extends RuntimeException {

    public NewsletterNotFoundException(Long id){
        super("Could not find newsletter " + id);
    }
}
